var pj_extractor = {
    debug: true,

    minGoodLinkSize: 20000,

    tagsAsWhole: ['tbody', 'thead', 'tfoot', 'th', 'tr', 'td', 'li'],
    
    decorationTags: ["p", "br", "font", "b", "i", "u", "center", "big", "small", "strong", "strike", "sub", "sup", "code", "em", "abbr", "acronym", "address", "blockquote", "cite", "dfn", "samp", "kdb", "var", "del", "ins", "ruby", "rt", "rp", "h1", "h2", "h3", "h4", "h5", "h6", "wbr"],
    
    statAttrs: function(e) {
        return 'tag: ' + e.tagName + ', w: ' + $(e).attr('_w')
             + ', h: ' + $(e).attr('_h')
             + ', s: ' + $(e).attr('_size')
             + ', c: ' + $(e).attr('_children')
             + ', d: ' + $(e).attr('_density');
    },
    
    absoluteUrls: function() {
        $('img').each(function () {
            $(this).attr('src', this.src);  
        });
    },
    
    removeUseless: function() {
        // remove comments
        $('*').contents().filter(function () {
            return this.nodeType == 8;
        }).remove()

        // remove position:absolute elements
        $('body').find('*').filter(function () {
            return $(this).css('position') == "absolute";
        }).remove();
        
        // remove hidden elements
        $('body').find(':hidden').filter(function() {
            return this.tagName.toLowerCase() != 'br';
        }).remove();
        
        var tags = ['script', 'noscript', 'link', 'applet', 'base', 'basefont', 'bgsound', 'button', 'embed', 'frame', 'frameset', 'iframe', 'input', 'isindex', 'label', 'marquee', 'menu', 'object', 'optgroup', 'option', 'param', 'select', 'style', 'textarea'];
        for (var i = 0; i < tags.length; i++)
            $(tags[i]).remove();
                    
        // remove id/class/style attrs
        var attrs = ['id', 'class', 'style', 'target'];
        for (var i = 0; i < attrs.length; i++)
            $('*').removeAttr(attrs[i]);
    },
    
    removeEmpty: function() {
        $('body').find('[_size=0]').filter(function () {
            var tag = this.tagName.toLowerCase();
            return pj_extractor.tagsAsWhole.indexOf(tag) == -1 && tag != 'br';
        }).remove();
    },

    removeAttrs: function() {
        $('*').each(function () {
            var attrs = [];
            $.each(this.attributes, function() {
                if (this.name.indexOf('_') == 0 || this.name.indexOf('on') == 0)
                    attrs.push(this.name);
            });
            for (var i = 0; i < attrs.length; i++)
                $(this).removeAttr(attrs[i]);
        });
    },
    
    /* calculate density for every element
    we have 2 methods to calculate content size of an element:
    1. use area size (height * width), which is not very accurate (some h or w might be 0)
    2. use character count, which may treat &gt; as 4 chars and &#8482; as 7 chars and unfair for Chinese and English chars
    we'll use area size as content size, but have to calculate area size bottom-up recursively,
    also, we'll calculate twice, 1st pass before removing CSSes, 2nd pass after removing useless elements
    
    we can use children element count as the density unit, so:
    density = content-size / children-element-count
    
    but we must note:
    1. decoration-tag elements should NOT count as elements
    2. some elements should count more than 1 element: a
    3. since leaf-elements don't have child elements, so to be equal every element's child element count should plus 1
    */
    calcWidthHeight: function() {
        $('body').find('*').each(function () {
            var self = $(this);
            var h = self.height(), w = self.width();
            var oldH = self.attr('_h'), oldW = self.attr('_w');
            oldH = oldH == undefined ? 0 : parseInt(oldH);
            oldW = oldW == undefined ? 0 : parseInt(oldW);
            
            if (oldH == 0 || oldH > h)
                self.attr('_h', h);
            if (oldW == 0 || oldW > w)
                self.attr('_w', w);
        });
    },
    
    textNodeSize: function(tn) {
        var range = document.createRange();
        range.selectNodeContents(tn);
        var rect = range.getBoundingClientRect();
        return (+rect.height) * (+rect.width);
    },
        
    calcContentSize: function() {
        while (1) {
            var noSized = $('body').find(':not([_size])').filter(function() {
                if ($(this).children().length == 0) {
                    var w = parseInt($(this).attr('_w'));
                    var h = parseInt($(this).attr('_h'));
                    $(this).attr('_size', w * h);
                    return false;
                }

                var total = 0;
                $(this).contents().each(function () {
                    if (this.nodeType == 3) {  // text-node
                        total += pj_extractor.textNodeSize(this);
                    } else {
                        if ($(this).attr('_size') == undefined) {
                            total = -1;
                            return false;  // break out of each loop
                        }
                        total += parseInt($(this).attr('_size'));
                    }
                });

                if (total < 0)
                    return true;

                $(this).attr('_size', total);
                return false;
            });

            if (noSized.length == 0)
                break;
        }

        var total = 0;
        $('body').contents().each(function() {
            if (this.nodeType == 3) {  // text-node
                total += pj_extractor.textNodeSize(this);
            } else {
                total += parseInt($(this).attr('_size'));
            }
        });
        $('body').attr('_size', total);
    },
    
    calcDensity: function() {
        // calculate every elements child-element count
        $('body').find('*').each(function () {
           var effective = $(this).find('*').filter(function () {
               return pj_extractor.decorationTags.indexOf(this.tagName.toLowerCase()) == -1;
           });
           
           var links = $(this).find('a');
           var goodLinks = $(this).find('a').filter(function () {
               var size = +($(this).attr('_size'));
               return size > pj_extractor.minGoodLinkSize;
           });
           
           var count = effective.length + links.length - 2*goodLinks.length + 1;
           var size = parseInt($(this).attr('_size'));
           $(this).attr('_children', count);
           $(this).attr('_density', size / count);
        });
    },
    
    /* select most appropriate element
    1. select all those elements whose content-size > 2/5 of body's content-size
    2. select from those elements whose density is the biggest
    */
    selectBlock: function() {
        var total = parseInt($('body').attr('_size'));
        if (this.debug)
            console.log('total size: ' + total);
        
        var bigs = $('body').find('*').filter(function () {
            return parseInt($(this).attr('_size')) * 5 > 2 * total;
        }).sort(function (a, b) {
            return parseInt($(a).attr('_density')) - parseInt($(b).attr('_density'));
        });
      
        if (0 == bigs.length)
            return null;
        
        if (this.debug)
        for (var i = 0; i < bigs.length; i++)
            console.log('bigs[' + i + ']: ' + this.statAttrs(bigs[i]));
             
        var base = bigs[bigs.length-1];
        return this.trim(this.shrinkSingle(this.tryParent(base), base));
    },
    
    linkSize: function(element) {
        var total = 0;
        $(element).find('a').each(function () {
            var size = parseInt($(this).attr('_size'));
            total += size <= pj_extractor.minGoodLinkSize ? size : 0; 
        });
        return total;
    },
    
    tooManyLinks: function(element) {
        if ($(element).attr('_size') == undefined)  // text node
            return false;
            
        return this.linkSize(element) * 3 > parseInt($(element).attr('_size'));
    },
    
    trim: function(block) {
        // remove empty text-node
        $(block).contents().filter(function () {
            return this.nodeType == 3 && $(this).text().trim().length == 0;    
        }).remove();
        
        var toCrops = [];
        $(block).contents().each(function (i) {
            if (pj_extractor.tooManyLinks(this))
                toCrops.push(i);
        });
        
        if (this.debug)
            console.log('toCrops: ' + toCrops + ', total: ' + $(block).contents().length);
        
        // trim tail
        if (toCrops.length > 0 && toCrops[toCrops.length - 1] == $(block).contents().length-1) {
            for (var i = toCrops.length-1; i >=0; i--) {
                var contents = $(block).contents();
                $(contents[contents.length-1]).remove();
                if (i >= 1 && toCrops[i]+1 != toCrops[i-1])
                    break;
            }
        }
        
        // trim head
        if (toCrops.length > 0 && toCrops[0] == 0)
        for (var i = 0; i < toCrops.length; i++) {
            $($(block).contents()[0]).remove();
            if (i+1 < toCrops.length && toCrops[i]+1 != toCrops[i+1])
                break;
        }
        
        // trim non-decoration elements, but elements such as li/td/tr/thead/tbody/th/tfoot can't be deleted individually
        $(block).children().filter(function () {
            if (pj_extractor.decorationTags.indexOf(this.tagName.toLowerCase()) >= 0)
                return false;
            
            if (pj_extractor.tooManyLinks(this))
                return true;
            
            $(this).find('*').filter(function () {
                return pj_extractor.tagsAsWhole.indexOf(this.tagName.toLowerCase()) == -1 && pj_extractor.tooManyLinks(this); 
            }).remove();
            return false;
        }).remove();
        
        // trim empty elements except img/td/br
        while(1) {
            var empties = $(block).find('*').filter(function() {
                var tag = this.tagName.toLowerCase(); 
                return  "img" != tag && pj_extractor.tagsAsWhole.indexOf(tag) == -1 
                    && $(this).html().trim().length == 0
                    && this.tagName.toLowerCase() != 'br'; 
            }).remove();
            
            if (empties.length == 0)
                break;
        }

        // trim mostly empty ul/ol
        $(block).find('*').filter(function() {
            if (['ul', 'ol'].indexOf(this.tagName.toLowerCase()) == -1)
                return false;

            var empties = $(this).children().filter(function() {
                return $(this).html().trim().length == 0;
            });
            return empties.length*2 > $(this).children().length;
        }).remove();

        return block;
    },
    
    /* try to expand to parent
    as far as the current element is NOT body and 
    the siblings' link-area/content-size ratio do NOT differ much from the element's
    */
    tryParent: function(element) {
        if (element.tagName.toLowerCase() == 'body') {
            if (this.debug)
                console.log('body reached');
            return element;
        }
        
        var parent = $(element).parent();
        if (parent.children().length == 1) {
            if (this.debug)
                console.log('single child, try parent');
            return this.tryParent(parent[0]);
        }
        
        var parentSize = parseInt(parent.attr('_size'));
        var parentLinkSize = this.linkSize(parent[0]);
        var mySize = parseInt($(element).attr('_size'));
        var myLinkSize = this.linkSize(element);
        var siblingSize = parentSize - mySize;
        var siblingLinkSize = parentLinkSize - myLinkSize;
        
        // sibLinkSize / sibSize should NOT > 1.5 * (myLinkSize / mySize)
        // i.e, (2 * sls * ms) should be less than (3 * mls * ss)
        if (2 * siblingLinkSize * mySize > 3 * myLinkSize * siblingSize) {
            if (this.debug)
                console.log('sls/ss too large, ' + this.statAttrs(element)
                    + ', mls/ms: ' + myLinkSize + '/' + mySize
                    + ', sls/ss: ' + siblingLinkSize + '/' + siblingSize
                    );
            return element;
        }

        if (this.debug)
            console.log('sls/ss ok, ' + this.statAttrs(element)
                    + ', mls/ms: ' + myLinkSize + '/' + mySize
                    + ', sls/ss: ' + siblingLinkSize + '/' + siblingSize);
        return this.tryParent(parent[0]);  
    },
    
    shrinkSingle: function(root, base) {
        while ($(root).children().length == 1 && root != base && $(root).children()[0].tagName.toLowerCase() != "pre") {
            root = $(root).children()[0];
            if (this.debug)
                console.log('shrink single to ' + this.statAttrs(root));
        }
        return root;
    },

    // http://stackoverflow.com/questions/934012/get-image-data-in-javascript
    getBase64Image: function (img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);

        return canvas.toDataURL("image/png");
    },

    getLocalUrl: function (url) {
        var pathname = './imgs/' + url.split('://')[1];
        console.log('wget -c "' + url + '" -O "' + pathname + '"');
        return pathname;
    },

    extract: function(imgProc, debug) {
        this.debug = debug;
        if (debug)
            console.log('start extracting ' + document.location.href);
        this.calcWidthHeight();
        this.removeUseless();
        this.calcWidthHeight();
        this.calcContentSize();
        this.removeEmpty();
        this.calcDensity();
        var block = this.selectBlock();
        if (debug)
            console.log(null == block ? 'block null' : this.statAttrs(block));
        this.removeAttrs();
        this.absoluteUrls();

        if (null != block && imgProc != 0) {
            $(block).find('img').each(function() {
                if (this.src.indexOf('data:image') == 0)
                    return;

                if (imgProc == 2) { // local-img
                    var localUrl = pj_extractor.getLocalUrl(this.src);
                    $(this).attr('src', localUrl);
                } else  // embed-img
                try {
                    var dataUrl = pj_extractor.getBase64Image(this);
                    $(this).attr('src', dataUrl);
                } catch(e) {
                    console.log('base64 err: ' + e.message.substr(0, 100));
                }
            });
        }

        // var html = $(':root').html().replace(/^\s*\n/gm, "");
        // var html = $('body').html().replace(/^\s*\n/gm, "");
        var html = null == block ? '' : $(block).html().replace(/^\s*\n/gm, "");
        var title = document.title.length == 0 ? document.location.href : document.title;
        return {html: html, title: title};
    }
};