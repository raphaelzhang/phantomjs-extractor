"use strict";

var system = require('system'), webpage = require('webpage');

//imgProc: 0(def.)/leave as is, 1/embed as base64, 2/convert to local url
var imgProc = 0, execJs = true, indexer = false, urls = [];

for (var i = 1; i < system.args.length; i++)
    if (system.args[i] == '--embed-img')
        imgProc = 1;
    else if (system.args[i] == '--no-js')
        execJs = false;
    else if (system.args[i] == '--indexer')
        indexer = true;
    else if (system.args[i] == '--local-img')
        imgProc = 2;
    else
        urls.push(system.args[i]);

if (urls.length == 0) {
    console.log("Usage: " + system.args[0] + " [--embed-img] [--local-img] [--no-js] [--indexer] $URL [$URL1 [$URL2]] ...");
    phantom.exit(1);
}

var log = function(s) {
    console.log(new Date().fmt('yyyy-MM-dd HH:mm:ss') + ' ' + s);
};

var inject = function(obj, src, err) {
    if (!obj.injectJs(src)) {
        log('inject ' + src + ' error!');
        phantom.exit(err);
    }
};

inject(phantom, 'lib/URI.min.js', 2);
inject(phantom, 'utils.js', 3);
inject(phantom, 'builder.js', 4);

var page = webpage.create();

page.onConsoleMessage = function(msg) {
    log(msg);
};

page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36';
page.settings.webSecurityEnabled = false;
page.settings.resourceTimeout = 15000;

page.viewportSize = {width: 1440, height: 2560};
page.paperSize = {format: 'A4', orientation: 'portrait', margin: '1cm'};

page.onResourceRequested = function(req, network) {
    requestTracer.onRequest(req, network);
};

var progDone = function() {
    phantom.exit(0);
};

if (indexer)
    indexMultiple(page, urls, execJs, progDone);
else
    extractMultiple(page, urls, execJs, imgProc, null, progDone);