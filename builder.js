var pj_builder = {
    buildSingle: function(url, title, html) {
        return '<!DOCTYPE html>\n' + 
            '<html>\n' + 
            '<head>\n' +
            '    <meta charset="utf-8" />\n' + 
            '    <title>' + title + '</title>\n' +
            '    <style type="text/css" media="all">@import "../img/common.css";</style>\n' + 
            '    <style type="text/css" media="print">@import "../img/print.css";</style>\n' +
            '</head>\n' +
            '<body>\n' +
            '<div id=container>\n' +
            '<div id=header>\n' +
            '    <div class=title>' + title + '</div>\n' +
            '    <div class=nav><a target=_blank href="' + url + '">原网页</a> | <a href="javascript:window.print()">打印</a></div>\n' +
            '</div>\n' +
            '<div id=content>\n' +
            html + 
            '\n</div>\n' +
            '</div>\n' +
            '</body>\n' +
            '</html>';
    },

    buildToc: function(url, name, chapters) {
        var lines = parseInt(chapters.length / 3);
        if (lines * 3 < chapters.length)
            lines++;
             
        var html = '<table width="100%">\n';
        for (var i = 0; i < lines; i++) {
            var tdAttr = (0 == lines) ? ' width="33%"' : '';
            html += '  <tr>\n';
            for (var j = 3*i; j < 3*i+3; j++)
                if (j < chapters.length)
                    html += '    <td' + tdAttr + '><a href="' + (j+1) + '.html">' + chapters[j].name + '</a></td>\n';
                else
                    html += '    <td' + tdAttr + '></td>\n';
            html += '  </tr>\n';
        }
        html += '</table>';
        return this.buildSingle(url, name + '/' + chapters.length + '章', html);
    },

    buildChapter: function(url, title, html, current, maxChapters) {
        var prev = (current <= 1 ? 'index' : current-1) + '.html',
            next = (current == maxChapters ? 'index' : current+1) + '.html'; 
        return '<!DOCTYPE html>\n' + 
            '<html>\n' + 
            '<head>\n' +
            '    <meta charset="utf-8" />\n' + 
            '    <title>' + title + '</title>\n' +
            '    <style type="text/css" media="all">@import "../img/novel.css";</style>\n' + 
            '</head>\n' +
            '<body>\n' +
            '<div id=container>\n' +
            '<div id=header>\n' +
            '    <div class=title>' + title + '</div>\n' +
            '    <div class=nav><a target=_blank href="' + url + '">原网页</a></div>\n' +
            '</div>\n' +
            '<div id=content_wrap>\n' +
            '<div id=novelnav>\n' +
            '    <a class=toc href="index.html"></a>\n' +
            '    <a class=prevchapter href="' + prev + '"></a>\n' +
            '    <a class=nextchapter href="' + next + '"></a>\n' +
            '    <span>' + current + '</span>\n' +
            '    <span>' + maxChapters + '</span>\n' +
            '</div>\n' +
            '<div id=content>\n' +
            html + 
            '\n</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '</body>\n' +
            '<script>\n' +
            'document.body.onkeydown = function(ev) {\n' +
            '    var e = ev || window.event;\n' +
            '    if(e)\n' +
            '    {\n' +
            '        var key = window.event ? e.keyCode : e.which;\n' +
            '        switch(key) {\n' +
            '        case 13: //return\n' +
            '            document.location = "index.html";\n' +
            '            break;\n' +
            '        case 37: //left\n' +
            '            document.location = "' + prev + '";\n' +
            '            break;\n' +
            '        case 39: //right\n' +
            '            document.location = "' + next + '"\n' +
            '            break;\n' +
            '        }\n' +
            '    }\n' +
            '};\n' +
            '</script>\n' +
            '</html>';
    },

    buildFull: function(url, nameLinks, htmls) {
        var combined = '<div>\n' + htmls[0] + '</div>\n';
        for (var i = 1; i < htmls.length; i++)
            combined += '<div' + (i % 2 == 1 ? "" : ' style="background-color:#eee"') + '>\n<a name="' + i + '" style="text-decoration:none;color:#933">' + nameLinks.children[i-1].name + '</a><br>\n' + htmls[i] + '</div>\n';
        return this.buildSingle(url, nameLinks.name, combined);
    },

    buildFullPdf: function(nameLinks, htmls) {
        var html = '';
        for (var i = 1; i < htmls.length; i++)
            html += '<div' + (i % 2 == 1 ? "" : ' style="background-color:#eee"') + '>\n<div style="text-decoration:none;color:#933">' + nameLinks.children[i-1].name + '</div><br>\n' + htmls[i] + '</div>\n';
        return '<!DOCTYPE html>\n' +
            '<html>\n' +
            '<head>\n' +
            '    <meta charset="utf-8" />\n' +
            '    <style type="text/css" media="all">@import "../img/common.css";</style>\n' +
            '</head>\n' +
            '<body>\n' +
            '<div id=container>\n' +
            '<div id=content>\n' +
            html +
            '\n</div>\n' +
            '</div>\n' +
            '</body>\n' +
            '</html>';
    },

    buildFullText: function(nameLinks, texts) {
        var deco = function(alpha, size) {
            var text = '';
            for (var i = 0; i < size; i++)
                text += alpha;
            return text;
        };

        var full = deco('=', 20) + ' ' + nameLinks.name + ' ' + deco('=', 20) + '\n\n';
        for (var i = 0; i < texts.length; i++) {
            full += deco('-', 20) + ' ' + nameLinks.children[i].name + ' ' + deco('-', 20) + '\n\n';
            full += texts[i] + '\n\n';
        }
        return full;
    }
};