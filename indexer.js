var pj_indexer = {
    parentPath: '',

    getNovelName: function() {
        // get name from self link
        var name = '';
        $('a[href]').each(function() {
            if (this.href == document.location.href) {
                name = $(this).text().trim();
                return false;  // break each-loop
            }
        });
        if (name.length > 0) {
            console.log('title got from link: ' + name);
            return name;
        }
        
        // make guesses from title
        var parts = document.title.replace(/[,\|\/ \-_》《、]/g, '|').split(/\|/g);
        console.log('parts[' + + parts.length + ']: ' + parts.join(' | '));
        for (var i = 2; i < parts[0].length; i++) {
            var word = parts[0].substr(0, i);
            var match = 0, miss = 0; 
            for (var j = 1; j < parts.length; j++) {
                if (parts[j].indexOf(word) > -1)
                    match++;
                else
                    miss++;
            }
            console.log('word: ' + word + ', match: ' + match + ', miss: ' + miss);
            if (match == 0) {
                name = parts[0].substr(0, i-1);
                break;
            } 
        }
        return name;
    },
    
    isChildLink: function(url) {
        var path = new URI(url).pathname();
        if (this.parentPath.length == 0) {
            this.parentPath = new URI(document.location.href).pathname();
            this.parentPath = this.parentPath.substring(this.parentPath.lastIndexOf('/')+1, -1);
        }
        return path.indexOf(this.parentPath) == 0 && path != this.parentPath;
    },
    
    index: function() {
        var links = [];
        $('a[href]').each(function() {
            links.push({link: this.href, name: $(this).html()});
        });
        links.sort(function(a, b) {
            if (a.link.length == b.link.length)
                return a.link.localeCompare(b.link);
            else
                return a.link.length < b.link.length ? -1 : 1;
        });
        
        var children = [];
        for (var i = 0; i < links.length; i++) {
            if (!this.isChildLink(links[i].link))
                continue;

            var uniq = true;
            for (var j = 0; j < children.length; j++) {
                if (children[j].link == links[i].link) {
                    uniq = false;
                    break;
                }
            }
            
            if (uniq)
                children.push({link: links[i].link, name: links[i].name});
        }
        return {name: this.getNovelName(), children: children}
    }
};