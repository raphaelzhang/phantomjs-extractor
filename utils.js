Date.prototype.fmt = function(fmt) {
	var o = {
	"M+" : this.getMonth()+1, //月份
	"d+" : this.getDate(), //日
	"h+" : this.getHours()%12 == 0 ? 12 : this.getHours()%12, //小时
	"H+" : this.getHours(), //小时
	"m+" : this.getMinutes(), //分
	"s+" : this.getSeconds(), //秒
	"q+" : Math.floor((this.getMonth()+3)/3), //季度
	"S" : this.getMilliseconds() //毫秒
	};
	var week = {
	"0" : "/u4e00",
	"1" : "/u4e00",
	"2" : "/u4e8c",
	"3" : "/u4e09",
	"4" : "/u56db",
	"5" : "/u4e94",
	"6" : "/u516d"
	};
	if(/(y+)/.test(fmt)){
		fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
	}
	if(/(E+)/.test(fmt)){
		fmt=fmt.replace(RegExp.$1, ((RegExp.$1.length>1) ? (RegExp.$1.length>2 ? "/u661f/u671f" : "/u5468") : "")+week[this.getDay()+""]);
	}
	for(var k in o){
		if(new RegExp("("+ k +")").test(fmt)){
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
		}
	}
	return fmt;
};

var requestTracer = {
    execJs: true,

    traceLog: true,

    adsDomains: ['.google-analytics.com/', '.googlesyndication.com/', '.doubleclick.net/', '/eclick.baidu.com/', '.cnzz.com/', '.baidustatic.com/', 'pos.baidu.com/', '/eclick.baidu.com/', '/cpro.baidu.com/', '/nsclick.baidu.com/', 'inte.sogou.com/', '.tanx.com/', '.revsci.net/', '/x.jd.com/', '.optimix.asia/', '.addthis.com/', '.wrating.com/', 'qeryz.com/'],

    onRequest: function(req, network) {
        for (var i = 0; i < this.adsDomains.length; i++) {
            if (req.url.indexOf(this.adsDomains[i]) > 1) {
                if (this.traceLog)
                    log('- ' + req.url.substr(0, 80));
                network.abort();
                return;
            }
        }

        if (!this.execJs) {
            var lower = (new URI(req.url)).pathname().toLowerCase();
            // if js, then forbid
            if (lower.lastIndexOf('.js') != -1 && lower.lastIndexOf('.js') + 3 == lower.length) {
                if (this.traceLog)
                    log('- ' + req.url.substr(0, 80));
                network.abort();
                return;
            }
        }

        if (this.traceLog)
            log('+ ' + (req.url.length > 256 ? req.url.substr(0, 256) + ' ... ...' : req.url));
    }
};

var extractOpened = function(page, execJs, imgProc, debug, done) {
    var extractDone = function() {
        var filtered = page.evaluate(function (ei, dbg) {
            return pj_extractor.extract(ei, dbg);
        }, imgProc, debug);
        page.stop();
        
        done(filtered.title, filtered.html);
    };

    if (!execJs) {
        extractDone();
        return;
    }

    // execjs
    var curTop = 0;
    var timer = setInterval(function() {
        var pageHeight = page.evaluate(function() {
            return document.body.scrollHeight;
        });

        if (debug)
            log('cur-top: ' + curTop + ', page-height: ' + pageHeight);

        if (pageHeight > curTop + page.viewportSize.height) {
            curTop += page.viewportSize.height/2;
            page.scrollPosition = {top: curTop, left: 0};
            return;
        }

        clearInterval(timer);
        extractDone();
    }, 3000);
};

var generateFull = function(page, url, nameLinks, folder, done) {
    var fs = require('fs'), htmls = [], texts = [];

    // we do NOT check page open error and ASSUME that every HTML is good
    var extractChapter = function(chapter) {
        if (chapter >= nameLinks.children.length) {
            var html = pj_builder.buildFull(url, nameLinks, htmls);
            fs.write(folder + 'full.html', html, 'w');

            var text = pj_builder.buildFullText(nameLinks, texts);
            fs.write(folder + 'full.txt', text, 'w');

            page.content = pj_builder.buildFullPdf(nameLinks, htmls);
            page.render(folder + 'full.pdf', {format: 'pdf', quality: '100'});

            log(folder + 'full.html(txt, pdf) saved');
            done();
            return;
        }

        page.open(encodeURI(folder + (chapter+1) + '.html'), function(status) {
            inject(page, 'lib/jquery-2.2.0.min.js', 11);

            var htmlText = (page.evaluate(function() {
                var textContent = function(element) {
                    var text = '';
                    $(element).contents().each(function() {
                        text += this.nodeType == 1 ? textContent(this) + '\n' : $(this).text();
                    });
                    return text;
                };
                var full = textContent($('#content')[0]);
                return [$('#content').html(), full.trim()];
            }));
            page.stop();
            htmls.push(htmlText[0]);
            texts.push(htmlText[1]);
            extractChapter(chapter+1);
        });
    };

    page.open(encodeURI(folder + 'index.html'), function(status) {
        inject(page, 'lib/jquery-2.2.0.min.js', 11);

        htmls.push(page.evaluate(function() {
            $('#content a').each(function(i) {
                $(this).attr('href', '#' + (i+1));
            });
            return $('#content').html();
        }));
        page.stop();
        extractChapter(0);
    });
};

var indexOpened = function(page, url, execJs, done) {
    var nameLinks = page.evaluate(function() {
        return pj_indexer.index();
    });
    page.stop();

    var fs = require('fs');
    var folder = './output/' + nameLinks.name + '_' + (new URI(url).hostname()) + '/';
    fs.makeTree(folder);
    var indexFile = folder + 'index.html';

    var extractChapters = function(newIndex) {
        if (newIndex) {
            var content = pj_builder.buildToc(url, nameLinks.name, nameLinks.children);
            fs.write(indexFile, content, 'w');
        }

        var links = [];
        for (var i = 0; i < nameLinks.children.length; i++)
            links.push(nameLinks.children[i].link);

        requestTracer.traceLog = false;
        extractMultiple(page, links, execJs, true, folder, function() {
            generateFull(page, url, nameLinks, folder, function() {
                requestTracer.traceLog = true;
                done();
            });
        });
    };

    if (!fs.isFile(indexFile)) {
        extractChapters(true);
        return;
    }

    // we assume local index html can always be opened
    page.open(encodeURI(indexFile), function(status) {
        var maxChapter = page.evaluate(function() {
            var maxCh = -1, name = '', tags = document.getElementsByTagName('a');
            for (var i = 0; i < tags.length; i++) {
                var parts = tags[i].href.split('/');
                var match = parts[parts.length-1].split('.')[0].match(/^\d+$/);
                if (null != match && match.length > 0 && parseInt(match[0]) > maxCh) {
                    maxCh = parseInt(match[0]);
                    name = tags[i].text.trim();
                }
            }
            return [maxCh, name];
        });
        page.stop();

        log('local-max-chapters found: ' + maxChapter[0] + ', web-max-chapters: ' + nameLinks.children.length);
        if (nameLinks.children.length > maxChapter[0] && fs.isFile(folder + maxChapter[0] + '.html')) {
            log('local-last-chapter: ' + maxChapter[1] + ', web-last-chapter: ' + nameLinks.children[nameLinks.children.length-1].name);
            fs.remove(folder + maxChapter[0] + '.html');
        }

        extractChapters(nameLinks.children.length > maxChapter[0]);
    });
};

var indexMultiple = function(page, urls, execJs, allDone) {
    var indexSingle = function(urlIndex) {
        if (urlIndex >= urls.length) {
            allDone();
            return;
        }

        // indexer does NOT need execjs
        requestTracer.execJs = false;

        log('=== S === before page.open[' + (urlIndex+1) + '/' + urls.length + '] for ' + urls[urlIndex]);

        page.open(urls[urlIndex], function (status) {
            if (status != "success") {
                log('Cannot open ' + urls[urlIndex]);
                phantom.exit(10);
            }

            inject(page, 'lib/jquery-2.2.0.min.js', 11);
            inject(page, 'lib/URI.min.js', 12);
            inject(page, 'indexer.js', 13);

            log('before indexOpened for ' + urls[urlIndex]);
            indexOpened(page, urls[urlIndex], execJs, function() {
                indexSingle(urlIndex + 1);
            });
        });
    };

    indexSingle(0);
};

var extractMultiple = function(page, urls, execJs, imgProc, outputFolder, allDone) {
    var fs = require('fs');
    requestTracer.execJs = execJs;

    var extractSingle = function (urlIndex) {
        if (urlIndex >= urls.length) {
            allDone();
            return;
        }

        var url = urls[urlIndex];

        var outputFile = (null != outputFolder) ?
            outputFolder + '/' + (urlIndex+1) + '.html' :
            './output/' + (new URI(url).hostname().replace(/\./g, '_')) + '.html';

        if (outputFolder != null && fs.isFile(outputFile)) {
            // log('=== + === [' + (urlIndex+1) + '/' + urls.length + '] for ' + url + ' already fetched: ' + outputFile);
            extractSingle(urlIndex + 1);
            return;
        }

        page.scrollPosition = {top: 0, left: 0};

        log('=== S === before page.open[' + (urlIndex+1) + '/' + urls.length + '] for ' + url);

        page.open(url, function (status) {
            if (status != "success") {
                log('Cannot open ' + url);
                phantom.exit(10);
            }

            inject(page, 'lib/jquery-2.2.0.min.js', 11);
            inject(page, 'extractor.js', 12);

            if (null == outputFolder)
                log('before extractOpened for ' + url);

            extractOpened(page, execJs, imgProc, null == outputFolder, function(title, html) {
                var content = null == outputFolder ?
                    pj_builder.buildSingle(url, title, html) :
                    pj_builder.buildChapter(url, title, html, urlIndex+1, urls.length);
                fs.write(outputFile, content, 'w');
                log('--- D --- processing done for ' + url);
                extractSingle(urlIndex + 1);
            });
        });
    };

    extractSingle(0);
};